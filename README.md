
## About

A React Single-Page Application For An Educational Website

* Visit The Website From Here **[OpenSchool](https://lelouche01.github.io/OpenSchool/)**

## Usage

As it is a *React* app, you must run a few commands firstly to have the app run properly, so after downloading the repository and *cd* to the folder just type the following commands

* Install missing modules

    ```
    npm install 
    ```
* Run the react app

    ```
    npm start 
    ```

## Contct me

* Twitter : https://twitter.com/amait0u
* Instagram : https://www.instagram.com/amait0u
